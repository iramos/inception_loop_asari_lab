# MEI generation code

get_ipython().run_line_magic('load_ext', 'autoreload')
get_ipython().run_line_magic('autoreload', '2')


import torch
import numpy as np
from scipy import ndimage
from scipy import signal
from tqdm import tqdm
from nnfabrik.utility.nn_helpers import get_dims_for_loader_dict
import matplotlib.pyplot as plt
from varname import nameof

def prepare_data(dataloaders,dataset_name,dat):
    trainset = dataloaders['train']
    img_shape = get_dims_for_loader_dict(trainset)[dataset_name]['images']
    mu = dat.statistics['images']['all']['mean'].item()
    s = dat.statistics['images']['all']['std'].item()
    mu_eye = None
    mu_beh = None
    return trainset, img_shape, mu, mu_beh, mu_eye, s

def get_adj_model(models, neuron_id, mu_eye = None, mu_beh= None):
    def adj_model(x):
        resp = 0
        for m in models:
            resp = resp + m(x, eye_pos=mu_eye, behavior=mu_beh)[:,neuron_id]
        return resp / len(models)
    
    return adj_model
    

def roll(tensor, shift, axis):
    if shift == 0:
        return tensor

    if axis < 0:
        axis += tensor.dim()

    dim_size = tensor.size(axis)
    after_start = dim_size - shift
    if shift < 0:
        after_start = -shift
        shift = dim_size - abs(shift)

    before = tensor.narrow(axis, 0, dim_size - shift)
    after = tensor.narrow(axis, after_start, shift)
    return torch.cat([after, before], axis)

def batch_std(batch, keepdim=False, unbiased=True):
    """ Compute std for a batch of images. """
    std = batch.view(len(batch), -1).std(-1, unbiased=unbiased)
    if keepdim:
        std = std.view(len(batch), 1, 1, 1)
    return std

def fft_smooth(grad, factor=1/4):
    """
    Tones down the gradient with 1/sqrt(f) filter in the Fourier domain.
    Equivalent to low-pass filtering in the spatial domain.
    """
    if factor == 0:
        return grad
    #h, w = grad.size()[-2:]
    # grad = tf.transpose(grad, [0, 3, 1, 2])
    # grad_fft = tf.fft2d(tf.cast(grad, tf.complex64))
    h, w = grad.size()[-2:]
    # grad = tf.transpose(grad, [0, 3, 1, 2])
    # grad_fft = tf.fft2d(tf.cast(grad, tf.complex64))
    tw = np.minimum(np.arange(0, w), np.arange(w-1, -1, -1), dtype=np.float32)  # [-(w+2)//2:]
    th = np.minimum(np.arange(0, h), np.arange(h-1, -1, -1), dtype=np.float32)
    t = 1 / np.maximum(1.0, (tw[None, :] ** 2 + th[:, None] ** 2) ** (factor))
    F = grad.new_tensor(t / t.mean()).unsqueeze(0)
    pp = torch.fft.fft2(grad.data)
    return torch.fft.ifft2(pp * F)

def blur1(img, sigma):
    if sigma > 0:
        for d in range(len(img)):
            img[d] = ndimage.filters.gaussian_filter(img[d], sigma, order=0)
    return img

def blur_in_place(tensor, sigma):
    blurred = np.stack([blur1(im, sigma) for im in tensor.cpu().numpy()])
    tensor.copy_(torch.Tensor(blurred))

def make_step(net, src, step_size=1.5, sigma=None, precond=0, step_gain=1,
              blur=True, jitter=0, eps=1e-12, clip=True, bias=0.4, scale=0.224,
              train_norm=None, norm=None, add_loss=0, _eps=1e-12):
    """ Update src in place making a gradient ascent step in the output of net.
    Arguments:
        net (nn.Module or function): A backpropagatable function/module that receives
            images in (B x C x H x W) form and outputs a scalar value per image.
        src (torch.Tensor): Batch of images to update (B x C x H x W).
        step_size (float): Step size to use for the update: (im_old += step_size * grad)
        sigma (float): Standard deviation for gaussian smoothing (if used, see blur).
        precond (float): Strength of gradient smoothing.
        step_gain (float): Scaling factor for the step size.
        blur (boolean): Whether to blur the image after the update.
        jitter (int): Randomly shift the image this number of pixels before forwarding
            it through the network.
        eps (float): Small value to avoid division by zero.
        clip (boolean): Whether to clip the range of the image to be in [0, 255]
        train_norm (float): Decrease standard deviation of the image feed to the
            network to match this norm. Expressed in original pixel values. Unused if
            None
        norm (float): Decrease standard deviation of the image to match this norm after
            update. Expressed in z-scores. Unused if None
        add_loss (function): An additional term to add to the network activation before
            calling backward on it. Usually, some regularization.
    """
    if src.grad is not None:
        src.grad.zero_()

    # apply jitter shift
    if jitter > 0:
        ox, oy = np.random.randint(-jitter, jitter + 1, 2)  # use uniform distribution
        ox, oy = int(ox), int(oy)
        src.data = roll(roll(src.data, ox, -1), oy, -2)

    img = src
    if train_norm is not None and train_norm > 0.0:
        # normalize the image in backpropagatable manner
        img_idx = batch_std(src.data) + _eps > train_norm / scale  # images to update
        if img_idx.any():
            img = src.clone() # avoids overwriting original image but lets gradient through
            img[img_idx] = ((src[img_idx] / (batch_std(src[img_idx], keepdim=True) +
                                             _eps)) * (train_norm / scale))

    y = net(img)
    (y.mean() + add_loss).backward()

    grad = src.grad
    if precond > 0:
        grad = fft_smooth(grad, precond)
        grad = grad.real # added after deprecation of old torch.rfft() and torch.irfft() functions

    # src.data += (step_size / (batch_mean(torch.abs(grad.data), keepdim=True) + eps)) * (step_gain / 255) * grad.data
    src.data += (step_size / (torch.abs(grad.data).mean() + eps)) * (step_gain / 255) * grad.data
    # * both versions are equivalent for a single-image batch, for batches with more than
    # one image the first one is better but it drawns out the gradients that are spatially
    # wide; for instance a gradient of size 5 x 5 pixels all at amplitude 1 will produce a
    # higher change in an image of the batch than a gradient of size 20 x 20 all at
    # amplitude 1 in another. This is alright in most cases, but when generating diverse
    # images with min linkage (i.e, all images receive gradient from the signal and two
    # get the gradient from the diversity term) it drawns out the gradient generated from
    # the diversity term (because it is usually bigger spatially than the signal gradient)
    # and becomes hard to find very diverse images (i.e., increasing the diversity term
    # has no effect because the diversity gradient gets rescaled down to smaller values
    # than the signal gradient)
    # In any way, gradient mean is only used as normalization here and using the mean is
    # alright (also image generation works normally).

    #print(src.data.std() * scale)
    if norm is not None and norm > 0.0:
        data_idx = batch_std(src.data) + _eps > norm / scale
        src.data[data_idx] =  (src.data / (batch_std(src.data, keepdim=True) + _eps) * norm / scale)[data_idx]

    if jitter > 0:
        # undo the shift
        src.data = roll(roll(src.data, -ox, -1), -oy, -2)

    if clip:
        src.data = torch.clamp(src.data, -bias / scale, (255 - bias) / scale)

    if blur:
        blur_in_place(src.data, sigma)
        
def process(x, mu=0.4, sigma=0.224):
    """ Normalize and move channel dim in front of height and width"""
    x = (x - mu) / sigma
    if isinstance(x, torch.Tensor):
        return x.transpose(-1, -2).transpose(-2, -3)
    else:
        return np.moveaxis(x, -1, -3)
    
def unprocess(x, mu=0.4, sigma=0.224):
    """Inverse of process()"""
    x = x * sigma + mu
    if isinstance(x, torch.Tensor):
        return x.transpose(-3, -2).transpose(-2, -1)
    else:
        return np.moveaxis(x, -3, -1)


def deepdraw(net, base_img, octaves, random_crop=True, original_size=None,
             bias=None, scale=None, track=False, device='cuda', **step_params):
    """ Generate an image by iteratively optimizing activity of net.
    Arguments:
        net (nn.Module or function): A backpropagatable function/module that receives
            images in (B x C x H x W) form and outputs a scalar value per image.
        base_img (np.array): Initial image (h x w x c)
        octaves (list of dict): Configurations for each octave:
            n_iter (int): Number of iterations in this octave
            start_sigma (float): Initial standard deviation for gaussian smoothing (if
                used, see blur)
            end_sigma (float): Final standard deviation for gaussian smoothing (if used,
                see blur)
            start_step_size (float): Initial value of the step size used each iteration to
                update the image (im_old += step_size * grad).
            end_step_size (float): Initial value of the step size used each iteration to
                update the image (im_old += step_size * grad).
            (optionally) scale (float): If set, the image will be scaled using this factor
                during optimization. (Original image size is left unchanged).
        random_crop (boolean): If image to optimize is bigger than networks input image,
            optimize random crops of the image each iteration.
        original_size (triplet): (channel, height, width) expected by the network. If
            None, it uses base_img's.
        bias (float), scale (float): Values used for image normalization (at the very
            start of processing): (base_img - bias) / scale.
        device (torch.device or str): Device where the network is located.
        step_params (dict): A handful of optional parameters that are directly sent to
            make_step() (see docstring of make_step for a description).
    Returns:
        A h x w array. The optimized image.
    """
    # prepare base image
    image = process(base_img, mu=bias, sigma=scale)  # (3,224,224)

    # get input dimensions from net
    if original_size is None:
        print('getting image size:')
        c, w, h = image.shape[-3:]
    else:
        c, w, h = original_size

    print("starting drawing")

    src = torch.zeros(1, c, w, h, requires_grad=True, device=device)
    
    if track:
        # start figure to plot activation values at each 10 iterations
        fig, ax = plt.subplots(11, figsize=(16,14))
        x = np.linspace(0,octaves[0]['iter_n'],10)
        ax[0].set_xlabel('iter number')
        ax[0].set_ylabel('activation for generated MEI')
        ax[0].set_xlim(0,1000)
        pl_index = 1
    
    for e, o in enumerate(octaves):
        if 'scale' in o:
            # resize by o['scale'] if it exists
            image = ndimage.zoom(image, (1, o['scale'], o['scale']))
        _, imw, imh = image.shape
        for i in range(o['iter_n']):
            if imw > w:
                if random_crop:
                    # randomly select a crop
                    # ox = random.randint(0,imw-224)
                    # oy = random.randint(0,imh-224)
                    mid_x = (imw - w) / 2.
                    width_x = imw - w
                    ox = np.random.normal(mid_x, width_x * 0.3, 1)
                    ox = int(np.clip(ox, 0, imw - w))
                    mid_y = (imh - h) / 2.
                    width_y = imh - h
                    oy = np.random.normal(mid_y, width_y * 0.3, 1)
                    oy = int(np.clip(oy, 0, imh - h))
                    # insert the crop into src.data[0]
                    src.data[0].copy_(torch.Tensor(image[:, ox:ox + w, oy:oy + h]))
                else:
                    ox = int((imw - w) / 2)
                    oy = int((imh - h) / 2)
                    src.data[0].copy_(torch.Tensor(image[:, ox:ox + w, oy:oy + h]))
            else:
                ox = 0
                oy = 0
                src.data[0].copy_(torch.Tensor(image))

            sigma = o['start_sigma'] + ((o['end_sigma'] - o['start_sigma']) * i) / o['iter_n']
            step_size = o['start_step_size'] + ((o['end_step_size'] - o['start_step_size']) * i) / o['iter_n']

            make_step(net, src, bias=bias, scale=scale, sigma=sigma, step_size=step_size, **step_params)

            if i % 100 == 0:
                print('finished step %d in octave %d' % (i, e))
                if track:
                    ax[0].scatter(i,net(src).mean().cpu().detach().numpy())
                    ax[pl_index].imshow(src.cpu().detach().numpy()[0,0,:,:])
                    pl_index += 1

            # insert modified image back into original image (if necessary)
            image[:, ox:ox + w, oy:oy + h] = src.data[0].cpu().numpy()
            
            # stopping iterations if mean activation of net declines for generated image
            
            #mean_activation = net(src).mean()
            #if i>1 and mean_activation <= mean_activation_hist:
            #    image = image_hist
            #    break
            #else:
            #    image_hist = image
            #    mean_activation_hist = net(src).mean()
            #    continue
                
    # returning the resulting image
    return unprocess(image, mu=bias, sigma=scale)


def contrast_tuning(model, img, bias, scale, min_contrast=0.01, n=1000, linear=True, use_max_lim=False):
    mu = img.mean()
    delta = img - img.mean()
    vmax = delta.max()
    vmin = delta.min()

    min_pdist = delta[delta > 0].min()
    min_ndist = (-delta[delta < 0]).min()

    max_lim_gain = max((255 - mu) / min_pdist, mu / min_ndist)

    base_contrast = img.std()

    lim_contrast = 255 / (vmax - vmin) * base_contrast # maximum possible reachable contrast without clipping
    min_gain = min_contrast / base_contrast
    max_gain = min((255 - mu) / vmax, -mu / vmin)

    def run(x):
        with torch.no_grad():
            img = torch.Tensor(process(x[..., None], mu=bias, sigma=scale)[None, ...]).cuda()
            result = model(img)
        return result

    target = max_lim_gain if use_max_lim else max_gain

    if linear:
        gains = np.linspace(min_gain, target, n)
    else:
        gains = np.logspace(np.log10(min_gain), np.log10(target), n)
    vals = []
    cont = []

    for g in tqdm(gains):
        img = delta * g + mu
        img = np.clip(img, 0, 255)
        c = img.std()
        v = run(img).data.cpu().numpy()[0]
        cont.append(c)
        vals.append(v)

    vals = np.array(vals)
    cont = np.array(cont)

    return cont, vals, lim_contrast


class multi_MEI_class:
    """
    dataset_name        : string    # string with dataset_name of dataset used for training
    dat                 : object    # fileTreeDataset object with data schema of data
    dataloaders         : object    # dataloader object for dataset_name training data
    models              : object    # torch nn objects trained with dataset_name training data
    n_seeds             : int       # number of distinct seeded models used
    TargetUnit          : int       # index of model output neuron to generate MEI for
    track               : bool      # plot graphs of model activation and generated images during gradient ascent
    MEIParameter:
        iter_n              : int       # int number of iterations to run
        start_sigma         : float     # float starting sigma value
        end_sigma           : float     # float ending sigma value    
        start_step_size     : float     # float starting step size  
        end_step_size       : float     # float ending step size   
        precond             : float     # float strength of gradient preconditioning filter falloff 
        step_gain           : float     # float scaling of gradient steps
        jitter              : int       # int size of translational jittering
        blur                : bool      # bool whether to apply bluring or not 
        norm                : float     # float norm adjustment after step, negative to turn off
        train_norm          : float     # float norm adjustment during step, negative to turn off
    MEIProperties:
        n_seeds             : int       # number of distinct seeded models used
        mei                 : array     # most exciting images
        activation          : float     # activation at the MEI
        monotonic           : bool      # does activity increase monotonically with contrast
        max_contrast        : float     # contrast at which maximum activity is achieved
        max_activation      : float     # activation at the maximum contrast
        sat_contrast        : float     # contrast at which image would start saturating
        img_mean            : float     # mean luminance of the image
        lim_contrast        : float     # max reachable contrast without clipping

    """
    def __init__(self, dataset_name, dat, dataloaders, models, n_seeds, MEIParameter):
        self.dataset_name = dataset_name
        self.dat = dat
        self.dataloaders = dataloaders
        self.models = models
        self.n_seeds = n_seeds
        self.MEIParameter = MEIParameter
        self.MEIProperties = {}

    def generate(self, neuron_id, track = False):
        print('Working on neuron_id={}'.format(neuron_id))

        # get input statistics
        dataset, img_shape, bias, mu_beh, mu_eye, scale = prepare_data(dataloaders = self.dataloaders, dataset_name = self.dataset_name, dat = self.dat)
        print('Working with images with mu={}, sigma={}'.format(bias, scale))

        # get n instances of model for target unit
        select_models = self.models[0:self.n_seeds]
        adj_model = get_adj_model(select_models, neuron_id)

        params = self.MEIParameter
        blur = bool(params['blur'])
        jitter = int(params['jitter'])
        precond = float(params['precond'])
        step_gain = float(params['step_gain'])
        norm = float(params['norm'])
        train_norm = float(params['train_norm'])

        octaves = [
            {
                'iter_n': int(params['iter_n']),
                'start_sigma': float(params['start_sigma']),
                'end_sigma': float(params['end_sigma']),
                'start_step_size': float(params['start_step_size']),
                'end_step_size': float(params['end_step_size']),
            },
            ]

        # prepare initial image
        channels, original_h, original_w = img_shape[-3:]
        # the background color of the initial image
        background_color = np.float32([128] * channels)
        # generate initial random image
        gen_image = np.random.normal(background_color, 8, (original_h, original_w, channels))
        gen_image = np.clip(gen_image, 0, 255)

        # generate class visualization via octavewise gradient ascent
        gen_image = deepdraw(adj_model, gen_image, octaves, clip=True,
                             random_crop=False, blur=blur, jitter=jitter,
                             precond=precond, step_gain=step_gain,
                             bias=bias, scale=scale, norm=norm, train_norm=train_norm, track=track)

        mei = gen_image.squeeze()

        with torch.no_grad():
            img = torch.Tensor(process(gen_image, mu=bias, sigma=scale)[None, ...]).to('cuda')
            activation = adj_model(img).data.cpu().numpy()[0]

        cont, vals, lim_contrast = contrast_tuning(adj_model, mei, bias, scale)

        MEI = {'n_seeds' : self.n_seeds,
        'mei' : mei,     
        'activation'   : activation,       
        'monotonic' : bool(np.all(np.diff(vals) >= 0)),    
        'max_activation' : np.max(vals),      
        'max_contrast' : cont[np.argmax(vals)],           
        'sat_contrast' : np.max(cont),          
        'img_mean' : mei.mean(),           
        'lim_contrast' : lim_contrast,
        }

        self.MEIProperties['neuron_id : '+str(neuron_id)] = MEI
        
        return MEI['mei']