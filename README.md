# inception_loop_asari_lab

Repository of code developed to implement inception loops ([Walker et al. 2019 Nature Neuro](https://www.nature.com/articles/s41593-019-0517-x)) and generate MEIs used by the Asari Lab.

To install the environment with the modified code and custom code necessary to use the notebooks, follow the followig steps after installing conda.
Use the terminal or an Anaconda Prompt for the following steps:

1. Create the environment from the inception_loop_env.yml file:  
conda env create -f inception_loop_env.yml
The first line of the yml file sets the new environment's name.

2. Activate the new environment:   
conda activate inception_loop_env

3. Verify that the new environment was installed correctly:  
conda env list  
You can also use conda info --envs  


To train the CNN for the inception loop, refer to the notebook_V1_core_CNN.ipynb to use a network with a pre-trained core on V1 neuron responses from [Lurz et al. 2020 bioRxiv](https://www.biorxiv.org/content/10.1101/2020.10.05.326256v2)), to notebook_task_driven_core.ipynb to use a network with a pre-trained image classification core from TorchVision models or to notebook_train_core_CNN.ipynb to train the core from scratch. To generate MEIs, refer to notebook_MEI_gen.ipynb.

